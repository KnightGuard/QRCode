package com.way.activity;

import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

import com.way.qrcode.R;
import com.way.swipeback.SwipeBackActivity;

public class ResultActivity extends SwipeBackActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qrcode_result);
		String result = getIntent().getStringExtra("result");
		TextView tv = (TextView) findViewById(R.id.qrcode_result_text);
		tv.setText(result);
		Linkify.addLinks(tv, Linkify.ALL);
	}
}

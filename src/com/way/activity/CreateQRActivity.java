package com.way.activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.way.bean.Content;
import com.way.qrcode.R;
import com.way.swipeback.SwipeBackActivity;

public class CreateQRActivity extends SwipeBackActivity implements
		OnClickListener {
	private EditText mDateEditText, mPriceEditText, mAccessKeyEditText,
			mBankNameEditText, mCompaneEditText, mBankIdEditText,
			mNameEditText, mIdEditText;
	private Button mCreateQrcodeBtn;
	private View mContentView;
	private Gson mGson;
	private Animation mShakeAnimY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_qrcode_layout);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		initView();
		initData();
	}

	private void initData() {
		mGson = new Gson();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date(System.currentTimeMillis()));
		mDateEditText.setText(date);
		mShakeAnimY = AnimationUtils.loadAnimation(this, R.anim.et_shake_y);
	}

	private void initView() {
		mContentView = findViewById(R.id.content_view);
		mDateEditText = (EditText) findViewById(R.id.qr_date);
		mPriceEditText = (EditText) findViewById(R.id.qr_price);
		mAccessKeyEditText = (EditText) findViewById(R.id.qr_access_key);
		mBankNameEditText = (EditText) findViewById(R.id.qr_bank_name);
		mCompaneEditText = (EditText) findViewById(R.id.qr_compane);
		mBankIdEditText = (EditText) findViewById(R.id.qr_bankid);
		mNameEditText = (EditText) findViewById(R.id.qr_name);
		mIdEditText = (EditText) findViewById(R.id.qr_id);
		mCreateQrcodeBtn = (Button) findViewById(R.id.create_qrcode_btn);
		mCreateQrcodeBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.create_qrcode_btn:

			String date = mDateEditText.getText().toString();
			String price = mPriceEditText.getText().toString();
			String access_key = mAccessKeyEditText.getText().toString();
			String bank_name = mBankNameEditText.getText().toString();
			String compane = mCompaneEditText.getText().toString();
			String bank_id = mBankIdEditText.getText().toString();
			String name = mNameEditText.getText().toString();
			String id = mIdEditText.getText().toString();

			if (TextUtils.isEmpty(date)) {
				mContentView.startAnimation(mShakeAnimY);
				return;
			}

			Content content = new Content(date, price, access_key, bank_name,
					compane, bank_id, name, id);
			String result = mGson.toJson(content);
			// L.i("result = " + result);
			Intent i = new Intent(this, EditQRActivity.class);
			i.putExtra("content", result);
			startActivity(i);
			finish();
			break;

		default:
			break;
		}
	}
}

package com.way.bean;

import java.io.Serializable;

public class Content implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5838590277909216273L;

	private String date;
	private String price;

	private String access_key;
	private String bank_name;
	private String compane;
	private String bank_id;
	private String name;
	private String id;

	public Content() {
		super();
	}

	public Content(String date, String price, String access_key,
			String bank_name, String compane, String bank_id, String name,
			String id) {
		super();
		this.date = date;
		this.price = price;
		this.access_key = access_key;
		this.bank_name = bank_name;
		this.compane = compane;
		this.bank_id = bank_id;
		this.name = name;
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAccess_key() {
		return access_key;
	}

	public void setAccess_key(String access_key) {
		this.access_key = access_key;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getCompane() {
		return compane;
	}

	public void setCompane(String compane) {
		this.compane = compane;
	}

	public String getBank_id() {
		return bank_id;
	}

	public void setBank_id(String bank_id) {
		this.bank_id = bank_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
